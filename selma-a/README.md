# Maquette Selma A

[👁‍🗨 voir la démo](https://dis-project.gitlab.io/journee-etude/selma-a/)

![](panels.png)

![](planches.png)

## Pour le lancer sur sa machine

```bash
$ npx serve
```

## Corpus

[👁‍🗨 structure JSON retenue](corpus.json) — pour utilisation avec [d3.js (module `hierarchy`)](https://www.npmjs.com/package/d3-hierarchy).

### Organisation initiale

<details><pre>
PROGRAMME
	CLASSIQUE
	APPRENTISSAGE
		ASSIMILATION
		PRENDRE EN COMPTE L'ERREUR DANS LE CONTRÔLE
		CONTROLEUR CHAOTIQUE
			MODÈLE CHAOTIQUE
		APPRENTISSAGE MULTIMODAL
	ALGORYTHME
		Un algorithme est une suite finie et non ambiguë d’opérations ou d'instructions permettant
		de résoudre un problème ou d'obtenir un résultat
		Donald Knuth (1938-)
			finitude
				« Un algorithme doit toujours se terminer après un nombre fini d’étapes. »
			définition précise
				« Chaque étape d'un algorithme doit être définie précisément, les actions à transposer doivent être spécifiées rigoureusement et sans ambiguïté pour chaque cas. »
			entrées
				« [...] des quantités qui lui sont données avant qu'un algorithme ne commence. Ces entrées sont prises dans un ensemble d'objets spécifié. »
			sorties
				« [...] des quantités ayant une relation spécifiée avec les entrées. »
			rendement
				« [...] toutes les opérations que l'algorithme doit accomplir doivent être suffisamment basiques pour pouvoir être en principe réalisées dans une durée finie par un homme utilisant un papier et un crayon. »
		George Boolos (1940-1996)
			« Des instructions explicites pour déterminer le nième membre d'un ensemble, pour n un entier arbitrairement grand. De telles instructions sont données de façon bien explicite, sous une forme qui puisse être utilisée par une machine à calculer ou par un humain qui est capable de transposer des opérations très élémentaires en symboles. »
		Gérard Berry (1948-)
			« Un algorithme, c’est tout simplement une façon de décrire dans ses moindres détails comment procéder pour faire quelque chose5. Il se trouve que beaucoup d’actions mécaniques, toutes probablement, se prêtent bien à une telle décortication. Le but est d’évacuer la pensée du calcul, afin de le rendre exécutable par une machine numérique (ordinateur...). On ne travaille donc qu’avec un reflet numérique du système réel avec qui l’algorithme interagit. »
	COGNITION
	TRAITEMENT DE L'INFORMATION
		EN AVAL DU CONTROLEET DE LA PERCEPTION NEURONALE
			COMPUTATION MORPHOLOGIQUE
ENERGIE
	CLASSIQUE / ELECTRIQUE
	ALTERNATIVE
		ALCOOL
		SOLAIRE
	CRÉATION
	RECUPERATION
	batterie au zinc
		https://www.futura-sciences.com/tech/actualites/technologie-batteries-zinc-imprimables-ultrafines-souples-bon-marche-44376/
	Self-Folding Robots Work Without Batteries or Wires
		https://gizmodo.com/incredible-self-folding-robots-work-without-batteries-o-1797125283
MATERIAUX
	MATÉRIAUX QUI ONT DES PROPRIÉTÉS INTRINSÈQUES
		RAIDEUR
		ELASTICITÉ
		AMORTISSEMENT
		CONTRACTION
	MEMOIRE DE FORME
		CONTRACTION
			(note) TEMPS DE REPOS LONG
INTERACTIONS
	ENVIRONNEMENT
		SYSTEME DYNAMIQUE
	ECOSYSTEME
		EXPLORATION
		EXPLOITATION
	CAPTEURS
		FEEDBACK
			POSITIF
			NÉGATIF
		OBJET À COMPORTEMENT
			ENFERME DANS DES CATÉGORIES
				(note) QUALIFIER UN OBJET DE SUJET RÉDUIT LE SUJET À L'ÉTAT D'OBJET
			BEHAVIORALISME
				BROOKS
            OBJET HANTÉE / POSSÈDÉ
        	HYPEREALISME
        		(note) COINCE L'INTERACTION SI ELLE NE REPOND PAS CORRECTEMENT
			SYSTEME BASÉ SUR L'ERREUR
		PUBLIC
			EMPATHIE
FORME
	MATRICE
		SURFACE MOBILE
	MATERIAUX RECONFIGURABLES
		A toolkit for transformable materials
			https://techxplore.com/news/2017-01-materials-reprogrammable-function.html
		MÉTAMATÉRIAUX
			http://www.savoirs.essonne.fr/thematiques/la-matiere/physique/les-metamateriaux-plus-rien-ne-les-arrete/
		STRUCTURES DEPLOYABLES
			Ingestible origami robot
				http://news.mit.edu/2016/ingestible-origami-robot-0512
				https://www.boredpanda.com/self-folding-miniature-origami-robot-mit/?utm_source=r.search.yahoo&utm_medium=referral&utm_campaign=organic
			Self-Folding Robots Work Without Batteries or Wires
				https://gizmodo.com/incredible-self-folding-robots-work-without-batteries-o-1797125283
			Tribot MultiGait Origami Robot
				https://www.researchgate.net/figure/Fabrication-process-of-the-three-legged-multigait-origami-robot-Tribot-PCB-and-Kapton_fig3_322372411
			BotMakers, Unite! Origami Robots Could Start ‘Transformer’ Revolution
				https://www.nbcnews.com/science/science-news/botmakers-unite-origami-robots-could-start-transformer-revolution-n175151
			Swarm of Origami Robots
				https://spectrum.ieee.org/automaton/robotics/robotics-hardware/harvard-self-folding-origami-robots
		TENSÉGITÉ
			CORPS
				http://labs.compagnieinvitro.fr/wp-content/uploads/2016/01/11.png
			Biot project
				http://people.csail.mit.edu/edsinger/biot.htm
			Peristaltic Locomotion
				Continuous Wave Peristaltic Motion.pdf
				WORM-LIKE LOCOMOTION SYSTEMS.pdf
				Peristaltic Locomotion.pdf
				Single Motor Actuated Peristaltic Wave Generator.pdf
		MORPHOLOGIE
			MORPHOGENÈSE
			CALCUL MORPHOLOGIQUE
		MATÉRIAUX RESPONSIVE
			HYLÉMORPHISME
			PROCESSUS DE CROISSANCE
</pre></details>
