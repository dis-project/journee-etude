export const opacity = (depth) => ({
  '--opacity': 1 - (Math.log(depth) / 20),
  'background-color': `rgba(calc(var(--color-r) * 1.${depth-1}),calc(var(--color-g) * 1.${depth-1}),calc(var(--color-b) * 1.${depth-1}), var(--opacity))`,
})
