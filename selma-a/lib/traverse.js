export function nextSibblings (node) {
  let flag = false

  return node.parent.children.filter(n => {
    if (n === node) {
      flag = true
      return false
    }

    return flag
  })
}
