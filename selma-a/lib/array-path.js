export function nodePath (node) {
  return node
    .ancestors()
    .map(node => node.data.name)
    .reverse()
    .slice(1)
    .join('/')
}

export function pathId (path) {
  return objectHash(path.split('/'))
}
