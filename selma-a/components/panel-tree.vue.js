export default { install (Vue, options) {
  Vue.component('panel-tree', {
    props: {
      panel: {
        type: Object,
        required: true,
      }
    },

    render (html) {
      const {panel} = this;

      return html`<div class="panel-inner">
        <${'h'+panel.depth}>${panel.data.name} (${(panel.children || []).length})</${'h'+panel.depth}>

        <node-meta node=${panel}></node-meta>
        <panel-list panel=${panel}></panel-list>
      </div>`
    },
  })
} }
