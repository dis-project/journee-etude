import {opacity} from '../lib/colors.js'

export default { install (Vue, options) {
  Vue.component('panel-list', {
    props: {
      panel: {
        type: Object,
        required: true,
      }
    },

    render (html) {
      const {panel} = this;

      return html`<ul class="panels-list">
        ${(panel.children || []).map(panel => html`
          <li data-level=${panel.depth} data-id=${panel.data.id} style=${opacity(panel.depth)}>
            <${'h'+panel.depth}>
              <a href=${'#!/corpus/' + panel.data.path}>
                ${panel.data.name} (${(panel.children || []).length})
              </a>
            </${'h'+panel.depth}>

            <ul class="items-list">
              ${(panel.children || []).map(panel => html`
              <li class="item" data-id=${panel.data.id} style=${opacity(panel.depth)}>
                <a href=${'#!/corpus/' + panel.data.path}>${panel.data.name || '-'}</a>
              </li>
              `)}
            </ul>
          </li>
        `)}
      </ul>`
    },
  })
} }
