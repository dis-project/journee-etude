export default { install (Vue, options) {
  Vue.component('node-siblings', {
    props: {
      nodes: {
        type: Array,
        default: () => [],
      }
    },

    render (html) {
      const {nodes} = this;

      return html`<ul class="node-siblings">
        ${nodes.map(node => html`<li>
          <a href=${'#!/corpus/' + node.data.path}>
            ${node.data.name}
          </a>
        </li>`)}
      </ul>`
    },
  })
} }
