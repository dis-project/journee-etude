import cx from 'https://cdn.jsdelivr.net/npm/clsx@1/dist/clsx.m.js'
import {opacity} from '../lib/colors.js'

export default { install (Vue, options) {
  Vue.component('hierarchy', {
    props: {
      root: {
        type: Object,
        required: true,
      },

      activeNode: Object,
      path: String
    },

    methods: {
      inNode (activeNode, currentNode) {
        if (!activeNode) {
          return false;
        }

        return Boolean(activeNode.ancestors().find(n => n === currentNode))
      }
    },

    render (html) {
      const inNode = this.root.children.some(n => this.inNode(this.activeNode, n))

      return html`<main id="hierarchy">
        <ul class=${cx({ 'panels-list': true, 'with-active-panel': inNode })}>
          ${this.root.children.map(panel => {
            const inNode = this.inNode(this.activeNode, panel)
            const classes = cx({ panel: true, 'panel--active': inNode })
            const component = inNode
              ? Vue.options.components['panel-content']
              : Vue.options.components['panel-tree']

            const depth = (inNode ? this.activeNode : panel).depth
            const id = (inNode ? this.activeNode : panel).id

            return html`
              <li class=${classes} data-level=${depth} data-id=${id} style=${opacity(depth)}>
                <${component} panel=${panel} node=${this.activeNode}></${component}>
              </li>
            `
          })}
        </ul>
      </main>`
    },
  })
} }
