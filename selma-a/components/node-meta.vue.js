export default { install (Vue, options) {
  Vue.component('node-meta', {
    props: {
      node: {
        type: Object,
        required: true,
      }
    },

    render (html) {
      const {node} = this;

      return html`<div class="node-meta">
        ${node.data.dates && html `<div class="meta--dates">
          📅 ${node.data.dates}
        </div>`}

        ${node.data.description && html `<div class="meta--description">
          ${node.data.description}
        </div>`}
      </div>`
    },
  })
} }
