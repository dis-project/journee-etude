import {nextSibblings} from '../lib/traverse.js';

export default { install (Vue, options) {
  Vue.component('panel-content', {
    props: {
      panel: {
        type: Object || undefined,
        required: true,
      },

      node: {
        type: Object,
        required: true,
      }
    },

    render (html) {
      const {panel, node} = this;

      return html`<div class="panel-inner">
        <div class="panel-inner-main">
          <${'h'+node.depth} class="h1">
            ${node.data.name} (${(node.children || []).length})
          </${'h'+node.depth}>

          <node-meta node=${node}></node-meta>
          <panel-list panel=${node}></panel-list>
        </div>
        <node-siblings class="siblings--next" nodes=${nextSibblings(node)}></node-siblings>
      </div>`
    },
  })
} }
