export default { install (Vue, options) {
  Vue.component('board', {
    props: {
      board: {
        type: Object,
        default: () => ({ items: [] }),
      },
      hierarchy: {
        type: Object,
        default: () => ({}),
      }
    },

    data: () => ({
      prevX: null,
      prevY: null,
      initialX: null,
      initialY: null,
      initialH: null,
      initialW: null,
    }),

    components: {
      FreeTransform,
    },

    methods: {
      drag (boardItem, {x, y, scaleX, scaleY}) {
        const ok = Number.isFinite
        const {prevX, prevY, initialW, initialH} = this
        console.log({x, y, scaleX, scaleY, prevX, prevY})

        // first try
        if (prevX === null || prevY === null) {
          this.prevX = (scaleX > 1) ? x : boardItem.x
          this.prevY = (scaleY > 1) ? y : boardItem.y
          this.initialX = boardItem.x
          this.initialY = boardItem.y
          this.initialH = boardItem.h
          this.initialW = boardItem.w
          return
        }

        // resizing X from the right, rightwards
        if (ok(scaleX) && scaleX > 1 && x > prevX) {
          boardItem.w = boardItem.w + (x - prevX)
        }
        else if (ok(scaleX) && x < prevX) {
          boardItem.w = boardItem.w - (prevX - x)
        }
        // resizing X from the left, both directions
        else if (x === prevX && ok(scaleX)) {
          boardItem.w = initialW * scaleX
          boardItem.x = prevX - (boardItem.w - initialW)
        }

        // resizing Y from the top, upwards
        if (ok(scaleY) && scaleY > 1 && y > prevY) {
          boardItem.h = boardItem.h + (y - prevY)
        }
        else if (ok(scaleX) && x < prevX) {
          boardItem.h = boardItem.h - (prevY - y)
        }
        // resizing Y from the left, both directions
        else if (y === prevY && ok(scaleY)) {
          boardItem.h = initialH * scaleY
          boardItem.y = prevY - (boardItem.h - initialH)
        }

        if (!ok(scaleX)) {
          boardItem.x = x
          boardItem.y = y
        }

        this.prevX = x
        this.prevY = y
      },

      release (event) {
        this.prevX = null
        this.prevY = null
        console.log('release')
      },
    },

    render (html) {
      return html`<aside>
        <h1>Board : ${this.board.name || '-' }</h1>

        <section class="board-items">
          ${this.board.items.map(item => {
            const node = this.hierarchy.descendants().find(n => n.data.path === item.path)

            return html`<${FreeTransform} tabindex="0" x=${item.x} y=${item.y} width=${item.w} height=${item.h} offsetX=${0} offsetY=${0} angle=${0} scaleX=${1} scaleY=${1} scaleFromCenter=${false} aspectRatio=${false} disableScale=${true} onUpdate=${$event => this.drag(item, $event)} onClick=${this.release}>
              <h2>${node.data.name}</h2>

              <p>${item.note}</p>

              <p><a href=${'#!/corpus/' + node.data.path}>⮐</a></p>
            </${FreeTransform}>`
          })}
        </section>
      </aside>`
    },
  })
} }
