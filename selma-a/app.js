import Vue from 'https://cdn.jsdelivr.net/npm/vue@2/dist/vue.esm.browser.js';
import HTML from 'https://cdn.jsdelivr.net/npm/vue-html@1/dist/html.es.js';
import page from 'https://cdn.jsdelivr.net/npm/page@1/page.mjs';

import Hierarchy from './components/hierarchy.vue.js';
import NodeMeta from './components/node-meta.vue.js';
import NodeSiblings from './components/node-siblings.vue.js';
import PanelList from './components/panel-list.vue.js';
import PanelTree from './components/panel-tree.vue.js';
import PanelContent from './components/panel-content.vue.js';
import Board from './components/board.vue.js';
import {nodePath, pathId} from './lib/array-path.js'

Vue.use(HTML)
Vue.use(Hierarchy)
Vue.use(NodeMeta)
Vue.use(NodeSiblings)
Vue.use(PanelList)
Vue.use(PanelTree)
Vue.use(PanelContent)
Vue.use(Board)

page('/', () => App.activePath = '')
page('/corpus/*', ({params}) => {
  App.activePath = params[0]
  App.activeBoardDisplayed = false
})
page('/board/:id', (ctx) => {
  App.activeBoard = Number(ctx.params.id)
  App.activeBoardDisplayed = true
})
page('*', (ctx) => {
  console.log(ctx);
})

page.base(location.pathname)

const App = new Vue({
  el: '#app',

  data: {
    root: { children: [] },
    boards: [],
    activeBoard: '',
    activeBoardDisplayed: false,
    activePath: '',
  },

  computed: {
    activePathHierarchy () {
      const node = this.activePathNode;

      if (!node) {
        throw new Error(`Node with path "${activePath}" could not be found`)
      }

      return node.ancestors().reverse().slice(1)
    },

    activePathNode () {
      if (!this.activePath) {
        return null
      }

      return this.root
        .descendants()
        .find(({data}) => data.path === this.activePath)
    },
  },

  methods: {
    toggleBoard () {
      if (!App.activeBoardDisplayed) {
        page(`#!/board/${App.activeBoard}`)
      }
      else {
        App.activeBoardDisplayed = false
      }
    }
  },
})

fetch('corpus.json')
  .then(response => response.json())
  .then(data => {
    App.root = d3.hierarchy(data.root)
      .count(d => (d.children || []).length)
      .each(node => {
        node.data.path = nodePath(node)
        node.data.id = pathId(node.data.path)
        return node
      })

    App.boards = data.planches
  })
  .then(() => page({ hashbang: true }))
  .catch(console.error)
